package com.efounder.push;

import com.xiaomi.mipush.sdk.MiPushMessage;

import java.io.Serializable;

/**
 * 推送消息的bean
 * Created by yqs97.
 * Date: 2020/1/13
 * Time: 10:17
 */
public class PushMessageBean implements Serializable {
    public static final int TYPE_XIAOMI = 0;
    public static final int TYPE_HUAWEI = 1;
    public static final int TYPE_GOOGLE = 2;

    /**
     * 推送类型小米谷歌华为
     */
    private int pushType;

    private MiPushMessage miPushMessage;

}
