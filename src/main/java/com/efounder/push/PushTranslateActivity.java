package com.efounder.push;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.util.BeforeLoginCheckUtils;
import com.huawei.android.hms.agent.R;
import com.utilcode.util.AppUtils;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * push消息中转界面
 *
 * @author YQS 2018 0607
 */
@Deprecated
public class PushTranslateActivity extends AppCompatActivity {

    public static final String TAG = "PushTranslateActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            Bundle bundle = getIntent().getExtras();

            if (!BeforeLoginCheckUtils.checkHasIMUserAccount() || !BeforeLoginCheckUtils.checkProfileisFull()) {
                //没有登录或者没有资源文件 打开应用首页
                AppUtils.launchApp(AppUtils.getAppPackageName());
                this.finish();
                return;
            }
            Class clazz = Class.forName(getResources().getString(R.string.from_group_backto_first));
            Intent intent = new Intent(this, clazz);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            this.finish();
        }
    }


}
