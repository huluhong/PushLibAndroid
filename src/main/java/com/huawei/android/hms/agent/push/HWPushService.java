//package com.huawei.android.hms.agent.push;
//
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.huawei.hms.push.HmsMessageService;
//import com.huawei.hms.push.RemoteMessage;
//import com.utilcode.util.ReflectUtils;
//
//import org.greenrobot.eventbus.EventBus;
//
///**
// * Created by yqs97.
// * Date: 2019/11/21
// * Time: 17:03
// */
//public class HWPushService extends HmsMessageService {
//
//    private static final String TAG = "HWPushService";
//    @Override
//    public void onMessageReceived(RemoteMessage message) {
//        Log.i(TAG, "onMessageReceived is called");
//
//        if (message.getData().length() > 0) {
//            Log.d(TAG, "Message data payload: " + message.getData());
//            if (/* This method callback must be completed within 10s. */ true) {
//                /* Otherwise, you need to start a new job for callback processing. */
//                startNewJobProcess();
//            } else {
//                // Process message within 10s
//                processNow();
//            }
//        }
//        // Check if this message contains a notification payload.
//        if (message.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + message.getNotification().getBody());
//        }
//        // TODO: your's other processing logic
//    }
//    private void startNewJobProcess() {
//        Log.d(TAG, "Start new job processing.");
//        //TODO:
//    }
//    private void processNow() {
//        Log.d(TAG, "Processing now.");
//    }
//
//
//    @Override
//    public void onNewToken(String token) {
//        Log.d("PushManager", TAG+" HUA WEI Refreshed token: " + token);
//        // send the token to your app server.
//        if (!TextUtils.isEmpty(token)) {
//            //这是收到的token
//            try {
//                //Class className = Class.forName("com.efounder.chat.event.PushTokenEvent");
//                Object object = ReflectUtils.reflect("com.efounder.chat.event.PushTokenEvent").newInstance(token).get();
//                EventBus.getDefault().post(object);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }        }
//    }
//
//}
